package com.example.test;

import org.testng.annotations.Test;

import static com.example.test.TestMethods.getPi;
import static org.testng.Assert.assertEquals;

public class TestCase {

    @Test
    public void piReturnsCorrectValue() {
        double expected = 3.14;
        System.out.println("expected: [" + expected + "], actual: [" + getPi() + "]");
        assertEquals(getPi(), expected, "Testing value of Pi");
    }
}
